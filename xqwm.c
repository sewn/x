#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

unsigned char *xprop(Display *dpy, Window win, char* name, Atom type, long size) {
	Atom restype;
	int status, format; // unused
	unsigned long nitems, after; // unused
	unsigned char *data;

	status = XGetWindowProperty(
		dpy, win, XInternAtom(dpy, name, False), 0, size,
		False, type, &restype, &format, &nitems, &after, &data
	);

	if (status != 0) {
		perror("XGetWindowProperty");
		exit(1);
	}

	return data;
}

Window xpropwin(Display *dpy, Window win, char* name) {
	return *((Window*)xprop(dpy, win, name, XA_WINDOW, sizeof(Window)));
}

char* xpropstr(Display *dpy, Window win, char* name) {
	return (char*)xprop(
		dpy, win, name, 
		XInternAtom(dpy, "UTF8_STRING", False), sizeof(char*)
	);
}

int main() {
	Display *dpy;
	char *wm;
	
	if (!(dpy = XOpenDisplay(NULL))) {
		perror("XOpenDisplay");
		exit(1);
	}

	wm = xpropstr(
		dpy,
		xpropwin(dpy, DefaultRootWindow(dpy), "_NET_SUPPORTING_WM_CHECK"),
		"_NET_WM_NAME"
	);

	printf("%s\n", wm);
	XCloseDisplay(dpy);

	return 0;
}
