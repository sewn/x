#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <signal.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/cursorfont.h>

static Atom atom_pid = -1;

static void usage(void) {
	fprintf(stderr, "%s\n", "usage: xpaw [-spk]");
	exit(1);
}

static int get_window_pid(Display *dpy, Window window) {
	Atom type;
	int status, fmt, pid;
	unsigned long nitems;
	unsigned long bytesa; // unused
	unsigned char *prop;

	if (atom_pid == (Atom)-1)
		atom_pid = XInternAtom(dpy, "_NET_WM_PID", 0);

	status = XGetWindowProperty(
		dpy, window, atom_pid,
		0, (~0L), False, AnyPropertyType,
		&type, &fmt, &nitems, &bytesa, &prop
	);

	if (status != Success) {
		perror("XGetWindowProperty");
		return -1;
	}

	if (nitems > 0)
		pid = (int) *((unsigned long *)prop);

	free(prop);
	return pid;
}

static int select_window_mouse(Display *dpy, Window root) {
	Cursor cursor;
	XEvent e;
	int status = 0;

	cursor = XCreateFontCursor(dpy, XC_target);
	status = XGrabPointer(dpy, root, False, ButtonReleaseMask,
		GrabModeSync, GrabModeAsync, root, cursor, CurrentTime);

	if (status != Success) {
		perror("XGrabPointer");
		return -1;
	}

	XAllowEvents(dpy, SyncPointer, CurrentTime);
	XWindowEvent(dpy, root, ButtonReleaseMask, &e);
	XUngrabPointer(dpy, CurrentTime);
	XFreeCursor(dpy, cursor);

	if (e.xbutton.button != 1)
		return -1;

	return e.xbutton.subwindow;
}

int main(int argc, char **argv) {
	Display *dpy;
	Window root;
	int c, window, pid;
	int getpid = 0, getwindow = 0, dokill = 0;

	dpy = XOpenDisplay(NULL);
	root = DefaultRootWindow(dpy);

	if (argc < 2)
		usage();

	while ((c = getopt(argc, argv, "spk")) != EOF) {
		switch (c) {
			case 's': getwindow = 1; break;
			case 'p': getpid = 1; break;
			case 'k': getpid = 1; dokill = 1; break;
			default: usage();
		}
	}

	window = select_window_mouse(dpy, root);

	if (window == -1) 
		return 1;

	if (getwindow)
		printf("%i\n", window);

	if (getpid) {
		pid = get_window_pid(dpy, window);
		if (pid == -1) 
			return 1;

		printf("%i\n", pid);
	}

	if(dokill)
		kill(pid, 9);

	XSync(dpy, False);
	XCloseDisplay(dpy);
	return 0;
}
