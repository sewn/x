CPPFLAGS = -I/usr/X11R6/include -D_POSIX_C_SOURCE
CFLAGS   = -std=c99 -Wall -pedantic
LDFLAGS  = -L/usr/X11R6/lib -lX11

all: xtrans xqwm xpaw

clean:
	rm -f xqwm xtrans xsw
