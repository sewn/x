#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define COLOR(r, g, b) ((r << 16) + (g << 8) + b)

static void xsethints(Display *dpy, Window win, int width, int height) {
	XSizeHints *sh;
	XClassHint cl = { "xtrans", "xtrans" };

	sh = XAllocSizeHints();

	/* 
	 * this will allow it to keep a constrained window size,
	 * or keeping the correct flag ratio i suppose.
	 */
	sh->min_aspect.x = sh->max_aspect.x = width;
	sh->min_aspect.y = sh->max_aspect.y = height;
	sh->flags = PAspect;

	XStoreName(dpy, win, "xtrans");
	XSetWMProperties(dpy, win, NULL, NULL, NULL, 0, sh, NULL, &cl);
}

static void xdrawtrans(Display *dpy, Window root, Window win) {
	GC gc;
	XGCValues gcv;
	int x, y; // unused
	unsigned int b, d; // unused
	unsigned int w, h;

	gc = DefaultGC(dpy, DefaultScreen(dpy));
	XGetGeometry(dpy, win, &root, &x, &y, &w, &h, &b, &d);

	/* pink */
	gcv.foreground = COLOR(237, 165, 179);
	XChangeGC(dpy, gc, GCForeground, &gcv);
	XFillRectangle(dpy, win, gc, 0, h / 5, w, h * 3 / 5);

	/* white */
	gcv.foreground = COLOR(255, 255, 255);
	XChangeGC(dpy, gc, GCForeground, &gcv);
	XFillRectangle(dpy, win, gc, 0, h * 2 / 5, w, h * 1 / 5);
}

static void xloop(Display *dpy, Window root, Window win, KeySym key) {
	XEvent e;
	int quit = 0;

	for (;;) {
		XNextEvent(dpy, &e);
		switch (e.type) {
		case Expose:
			if (e.xexpose.count == 0)
				xdrawtrans(dpy, root, win);
			break;
		case KeyPress:
			/* quit on specified key given as argument */
			if (e.xkey.keycode == key)
				quit = 1;
			break;
		default:
			break;
		}
		if (quit)
			break;
	}
}

int main(int argc, char **argv) {
	Display *dpy;
	Window root, win;

	/* default values */
	unsigned int h = 512;
	unsigned int w = h * 5/3;

	if (!(dpy = XOpenDisplay(NULL))) {
		perror("XOpenDisplay");
		return 1;
	}

	root = DefaultRootWindow(dpy);

	/* create solid cyan color window first */
	win = XCreateWindow(
		dpy, root, 0, 0, w, h, 0, 0, 0, 0,
		CWBackPixel | CWEventMask,
		&(XSetWindowAttributes){
			.background_pixel = COLOR(89, 200, 243),
			.event_mask = ExposureMask | KeyPressMask
		}
	);
	
	xsethints(dpy, win, w, h);
	XMapWindow(dpy, win);
	xloop(dpy, root, win, XKeysymToKeycode(dpy, XK_q));
	XSync(dpy, False);
	XCloseDisplay(dpy);

	return 0;
}
